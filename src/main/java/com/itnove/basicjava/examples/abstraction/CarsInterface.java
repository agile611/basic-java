package com.itnove.basicjava.examples.abstraction;

public interface CarsInterface {

    public String speed = "100";

    public void engineStart(String engineType, boolean isKeyLess);

}
